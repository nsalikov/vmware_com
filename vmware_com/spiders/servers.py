# -*- coding: utf-8 -*-
import re
import js2xml
import scrapy


class ServersSpider(scrapy.Spider):
    name = 'servers'
    allowed_domains = ['vmware.com']
    start_urls = ['https://www.vmware.com/resources/compatibility/js/data_server.js']


    def parse(self, response):
        jstree = js2xml.parse(response.text)
        data = js2xml.make_dict(jstree.xpath('//assign[left//identifier[@name="compdata"]]/right/*')[0])

        for line in data:
            server_ids = line[0]
            cpu_id = line[3]

            for server_id in server_ids:
                url = f'https://www.vmware.com/resources/compatibility/detail.php?deviceCategory=server&productid={server_id}'
                yield scrapy.Request(url, callback=self.parse_server, meta={'data': line, 'id': server_id})

            url = f'https://www.vmware.com/resources/compatibility/detail.php?scat=cpu&productid={cpu_id}'
            yield scrapy.Request(url, callback=self.parse_cpu, meta={'data': line, 'id': cpu_id})


    def parse_server(self, response):
        d = {}

        d['url'] = response.url
        d['type'] = 'server'
        d['id'] = response.meta['id']
        d['cpu_id'] = response.meta['data'][3]

        d['specs'] = dict(parse_specs(response))
        d['specs']['ESXi'] = [s.strip() for s in response.css('.details_table_tab2 .vmwareProduct select#cmbSRel option::text').extract()]

        return d


    def parse_cpu(self, response):
        d = {}

        d['url'] = response.url
        d['type'] = 'cpu'
        d['id'] = response.meta['id']

        d['specs'] = dict(parse_specs(response))
        d['specs']['ESXi'] = [s.strip() for s in response.xpath('//*[@class="details_table_tab2"]//*[@class="releases_table"]//tr/td[child::b[contains(text(), "ESXi")]]/following-sibling::td/text()').extract()]
        d['specs']['Notes'] = [' '.join([s.strip() for s in td.xpath('.//text()').extract()]) for td in response.xpath('//tr[@class="details_table_title_row" and child::td[contains(text(), "Notes")]]/following-sibling::tr//table[@class="details_table_tab2"]//tr/td[@colspan=3]')]

        return d


def parse_specs(response):
    for tr in response.css('.details_table_tab1 tr'):
        key = ' '.join(tr.xpath('./th//text()').extract()).strip()
        key = re.sub('\s*:\s*$', '', key)

        value = [s.strip() for s in tr.xpath('./td[1]//text()').extract()]

        if not value:
            value = None
        elif len(value) == 1:
            value = value[0]

        yield key, value
