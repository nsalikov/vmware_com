import json

input_file = 'vmware.jl'
servers_output_file = 'vmware_servers.jl'
cpus_output_file = 'vmware_cpus.jl'

with open(input_file, 'r', encoding='utf-8') as fin:
    servers = open(servers_output_file, 'w', encoding='utf-8')
    cpus = open(cpus_output_file, 'w', encoding='utf-8')

    for line in fin:
        if '"type": "server"' in line:
            servers.write(line)
        elif '"type": "cpu"' in line:
            cpus.write(line)
        else:
            print(line)

    servers.close()
    cpus.close()
